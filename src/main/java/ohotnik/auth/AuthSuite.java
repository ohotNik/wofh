package ohotnik.auth;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.ProxyConfig;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import com.gargoylesoftware.htmlunit.util.Cookie;
import ohotnik.Const;

import java.io.IOException;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 29.10.13
 * Time: 15:46
 */
public class AuthSuite {

    public Set<Cookie> execute(String login, String password, String world) {
        WebClient webClient = new WebClient(BrowserVersion.CHROME);
        try {
            ProxyConfig proxyConfig = new ProxyConfig("proxy-itc-01", 3128);
            webClient.setProxyConfig(proxyConfig);
            webClient.setThrowExceptionOnScriptError(false);
            HtmlPage page = webClient.getPage(Const.PAGE_MAIN);
            HtmlInput log = (HtmlInput)page.getElementById("fldNameOrEmail");
            log.setValueAttribute(login);
            HtmlInput pas = (HtmlInput)page.getElementById("fldPassword");
            pas.setValueAttribute(password);
            HtmlOption htmlOption = (HtmlOption) page.getByXPath("//option[text()='" + Const.WORLD + "']").get(0);
            htmlOption.setSelected(true);
            HtmlForm htmlForm = (HtmlForm) page.getElementById("loginForm");
            HtmlSubmitInput htmlSubmitInput = (HtmlSubmitInput)htmlForm.getByXPath("//input[@value='Вход']").get(0);
            htmlSubmitInput.click();
            return webClient.getCookieManager().getCookies();
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new UnsupportedOperationException();
    }

}
