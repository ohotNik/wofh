package ohotnik;

import com.gargoylesoftware.htmlunit.util.Cookie;
import ohotnik.auth.AuthSuite;
import ohotnik.city.CitySuite;

import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 29.10.13
 * Time: 15:45
 */
public class MC {

    public static void main(String[] args) {
        AuthSuite authSuite = new AuthSuite();
        Set<Cookie> cookieSet = authSuite.execute(Const.LOGIN, Const.PASSWORD, Const.WORLD);
        CitySuite citySuite = new CitySuite();
        citySuite.getCities(cookieSet);
    }

}
