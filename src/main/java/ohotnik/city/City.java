package ohotnik.city;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 29.10.13
 * Time: 17:28
 */
public class City {

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
