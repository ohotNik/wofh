package ohotnik.city;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.ProxyConfig;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.Cookie;
import ohotnik.Const;

import java.io.IOException;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: OkhonchenkoAV
 * Date: 29.10.13
 * Time: 17:27
 */
public class CitySuite {

    public Set<City> getCities(Set<Cookie> cookies) {
        WebClient webClient = new WebClient(BrowserVersion.CHROME);
        for (Cookie cookie : cookies) {
            webClient.getCookieManager().addCookie(cookie);
        }
        try {
            ProxyConfig proxyConfig = new ProxyConfig("proxy-itc-01", 3128);
            webClient.setProxyConfig(proxyConfig);
            webClient.setThrowExceptionOnScriptError(false);
            webClient.addRequestHeader("Referer", "www.wofh.ru");
            HtmlPage page = webClient.getPage("http://" + Const.PAGE_WORLD + "/");
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new UnsupportedOperationException();
    }

}
